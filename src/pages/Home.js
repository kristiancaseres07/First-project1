import styled from "styled-components";
import {Col, Row, Container, Button, Card} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Banner from '../components/Banner';
import WhoWeAre from '../components/WhoWeAre';
import OurServices from '../components/OurServices';
import AboutUs from '../components/AboutUs';
import LetsDoThis from '../components/LetsDoThis';  

export default function Home () {

    const [getServices, setServices] = useState([])




    const data = [
    {
      imgUrl: `https://file.rendit.io/n/Unk4J2fGdcKhcERfAjIY.png`,
      title: 'WEB DEVELOPMENT',
      description: 'Start and grow your business with your own website, tool and app.' 
    },
    {
      imgUrl: `https://file.rendit.io/n/WHNim069nv8wKzZNMwmG.png`,
      title: 'VIDEO and PHOTO PRODUCTION',
      description: 'Stand out with amazing visuals to send your message across.' 
    },
    {
      imgUrl: `https://file.rendit.io/n/nG1IIEVpw0AnccCrzWRa.png`,
      title: 'SOCIAL MEDIA MANAGEMENT',
      description: 'Engage your audience with content tailor-fit to your brand.' 
    },
    {
      imgUrl: `https://file.rendit.io/n/9HUVGxijUilBRqDV9p4Y.svg`,
      title: 'COMMUNITY MANAGEMENT',
      description: 'Nurture your community as they grow with you through engagements.' 
    },
    ]





  useEffect(() => {


      setServices(data.map(product => {
    return (
        <OurServices key={product.id} serviceProp={product}/>
      )
    }))


}, [])

  return (
    <div>
      <Banner />
      <WhoWeAre />
          <Container fluid className="midnightBlue" id="services">
            <h1 className="pt-5 pb-3"
             style={{color: "#ffffff", fontSize: "40px",fontWeight: "700", fontFamily: "Poppins", lineHeight: "80px", textAlign: "center"}}
            >Our Services</h1>
              <Container style={{padding: "0px 0px 100px 5%"}}>
                <Row>
                  {getServices}
                </Row>
              </Container>
          </Container>
      <AboutUs />
      <LetsDoThis />
    </div>
  );
};


