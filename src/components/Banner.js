import {Col, Row, Container, Button, Card} from 'react-bootstrap';
import { useState } from 'react';


export default function Banner() {
	

	return (
		<div id="home">
          <div className="going-up">
            <div>
            	<h1 className="BannerText"> 
            		Simplified <div className="Highlight">digital</div> solutions for your growing business. 
            	</h1>
                <Button className="BannerButton">
              		GET A QUOTE
              	</Button>
            </div>
              <img className="goingUp-img" src={`https://file.rendit.io/n/lgM8GcmDnm5Oe2KbeDSt.png`}/>
          </div>        
      </div>

		)
}

