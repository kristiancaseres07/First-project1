

export default function LetsDoThis() {

	return (

			   <div className="letsDoThis-div">
		          <img className="letsDoThis-pic" src={`https://file.rendit.io/n/pcQe5QR9zd3f70wyEN5h.png`} />
		          <div className="letsDoThis-text">
		            <h1 className="letsDoThis-title pt-3">
		              Let’s do this <br />
		              together.
		            </h1>
		            <h1 className="letsDoThis-subTitle" >LOCATION:</h1>
		            <span className="letsDoThis-info" >Quezon City, Philippines</span>
		            <h1 className="letsDoThis-subTitle"  >CONTACT:</h1>
		            <span className="letsDoThis-info" >hello@parazodigital.com</span >
		          </div >
		      </div>

		)
}